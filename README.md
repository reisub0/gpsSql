To use, install Go runtime with [snaps](https://github.com/golang/go/wiki/Ubuntu)

And then, `go get -u gitlab.com/reisub0/gpsSql`


Then, run it with `./gpsSql`.

If you want to edit the Port or the server names please edit the source file `main.go` and recompile with `go build`

To set GOPATH and do the other things follow this guide 

For this, use the code at gitlab.com/reisub0/gpsSql

Since this is written in GoLang, we need to set up a few steps beforehand.

1. First, install Go

   ```bash
   snap install --classic go
   ```

2. Set GOPATH environment variable: Add this to .bash_profile

   ```bash
   export GOPATH="/root/.go"
   export PATH="$PATH:/root/.go/bin"
   ```

3. Reboot the system for the path changes to take effect.

4. If dep is installed, skip to step 5 (Install dep)
   Install the Dep GoLang package to help with the dependencies
  ```bash
  curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
  ```

5. We are ready to download the gpsSql package.

   ```bash
   go get -u gitlab.com/reisub0/gpsSql
   ```

6. Now to ensure all the dependencies are correct

   ```bash
   cd $GOPATH
   cd src/gitlab.com/reisub0/gpsSql
   dep ensure
   ```

7. Now we can install gpsSql by running (in the same directory)

8. Three Options to run it(`go run main.go` ,`go build` + `./gpsSql` and `go install` + `gpsSql`)
