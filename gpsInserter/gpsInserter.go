// This package does the task of parsing any given message and inserting it into the 3 tables rawData, log, and location_history_current
// This package is called from gpsSql's main
package gpsInserter

import (
	"database/sql"
	"log"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

const (
	// The query format for inserting into raw_data table
	rawDataQuery = "INSERT INTO raw_data (device_id, rawdata, insert_id) VALUES (?, ?, 0)"
	// The query format for inserting into location_history_current table
	currentQuery = "INSERT INTO location_history_current (device_id, created_date, fixTime, updated_date, lat_message, lon_message, speed, ignition_status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
	// The query format for inserting into log table
	logQuery = "INSERT INTO log (device_id, fixTime, lat_message, lon_message, speed, ignition_status) VALUES (?,?,?,?,?,?)"
)

// Inserts the parsed message from string and inserts it into the appropriate tables in db
func Insert(messageString *string, db *sql.DB) {
	// To include support for other protocols, simple determine which protocol it is here
	// and make a function call similar to AIS140
	if strings.Contains(*messageString, "CHVTS") {
		AIS140Insert(messageString, db)
	}
}

// This function is very similar to the AIS140 value file in PHP
// Please refer to the AIS140 protocol document
func AIS140Insert(messageString *string, db *sql.DB) {
	trimmed := strings.TrimRight(*messageString, "*")
	csvList := strings.Split(trimmed, ",")

	packetType := csvList[4]
	log.Printf("Packet Type = %s", packetType)

	if packetType != "NR" {
		log.Printf("Invalid packet type")
		return
	}
	// Packet := csvList[5]
	// lorh := csvList[6]
	imei := csvList[7]

	// For inserting into an SQL table, we must first prepare the query
	// Preparing the query might give an error if for example, the query is not a valid SQL query
	// Or if the table doesn't exist
	rawDataStmt, _ := db.Prepare(rawDataQuery)

	// When we execute the query it checks if the number of parameters in
	// the function is equal to the number of question marks in the query
	res, e := rawDataStmt.Exec(imei, *messageString)
	if e != nil {
		log.Printf("Failed to execute statement - %s", rawDataQuery)
		return
	}

	// To find out if we inserted successfully
	lastId, _ := res.LastInsertId()
	rowCnt, _ := res.RowsAffected()
	log.Printf("Inserted into raw_data successfully")
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)
	// registerNumber := csvList[8]
	// GPSFix := csvList[9]

	datee := csvList[10]
	timeee := csvList[11]
	// Date time parsing is a bit different in Golang
	// We have a so called example fixed date time which is used in all parse strings
	// Instead of something like YYYY-MM-DD we must put the exact date 2006-01-02
	// And instead of the time HH:mm:ss we must put the exact time 15:04:05
	const timeFormat = "150405"
	const dateFormat = "02012006"
	const timeStampFormat = dateFormat + ":" + timeFormat
	dateeTimeee := strings.Join([]string{datee, timeee}, ":")
	// Since the time coming will be UTC time, parse it in that location
	// So that it will be converted according to our timezone
	// So it will automatically add +0530
	timestamp, e := time.ParseInLocation(timeStampFormat, dateeTimeee, time.UTC)
	if e != nil {
		log.Printf("%s", e)
		return
	}

	// Latitude & Longitude parsing
	latMessage := csvList[12]
	northDirection := csvList[13]
	parsedLat := 0.0
	if lat, e := strconv.ParseFloat(latMessage, 64); e != nil {
		log.Printf("Parsing error for latitude %s", latMessage)
		log.Printf("%s", e)
		return
	} else {
		parsedLat = float64(lat)
	}
	if northDirection == "S" {
		parsedLat = -parsedLat
	}

	lonMessage := csvList[14]
	eastDirection := csvList[15]
	parsedLon := 0.0
	if lon, e := strconv.ParseFloat(lonMessage, 64); e != nil {
		log.Printf("Parsing error for longitude %s", lonMessage)
		log.Printf("%s", e)
		return
	} else {
		parsedLon = float64(lon)
	}
	if eastDirection == "W" {
		parsedLon = -parsedLon
	}

	// Speed parsing
	speed := csvList[16]
	var parsedSpeed = 0.0
	_ = parsedSpeed
	if sp, e := strconv.ParseFloat(speed, 64); e != nil {
		log.Printf("Parsing error for speed %s", speed)
		log.Printf("%s", e)
		return
	} else {
		parsedSpeed = float64(sp)
	}

	// heading := csvList[17]
	// noOfSats := csvList[18]
	// altitude := csvList[19]
	// PDOP := csvList[20]
	// HDOP := csvList[21]
	// networkOperator := csvList[22]
	ignitionStatus := csvList[23]
	var parsedIgnition = 0
	if ig, e := strconv.ParseInt(ignitionStatus, 2, 64); e != nil {
		log.Printf("Parsing error for ignition %s", ignitionStatus)
		return
	} else {
		parsedIgnition = int(ig)
	}
	// batteryStatus := csvList[24]
	// batteryVoltage := csvList[25]
	// intBatteryVoltage := csvList[26]
	// This is the statement to insert into location_history_current
	currentStmt, e := db.Prepare(currentQuery)
	if e != nil {
		log.Printf("Failed to prepare statement - %s", currentQuery)
		log.Printf("%s", e)
		return
	}
	currentStmt.Exec(imei, time.Now(), timestamp, time.Now(), parsedLat, parsedLon, parsedSpeed, parsedIgnition)
	if e != nil {
		log.Printf("Failed to execute statement - %s", currentQuery)
		log.Printf("%s", e)
		return
	}
	lastId, _ = res.LastInsertId()
	rowCnt, _ = res.RowsAffected()
	log.Printf("INSERT INTO location_history_current (device_id, created_date, fixTime, updated_date, lat_message, lon_message, speed, ignition_status) VALUES (%s,%s,%s,%s,%f,%f,%f,%v)", imei, time.Now(), timestamp, time.Now(), parsedLat, parsedLon, parsedSpeed, parsedIgnition)
	log.Printf("Inserted into location_history_current successfully")
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)

	logStmt, e := db.Prepare(logQuery)
	if e != nil {
		log.Printf("Failed to prepare statement - %s", logQuery)
		log.Printf("%s", e)
		return
	}
	logStmt.Exec(imei, timestamp, parsedLat, parsedLon, parsedSpeed, parsedIgnition)
	if e != nil {
		log.Printf("Failed to execute statement - %s", logQuery)
		log.Printf("%s", e)
		return
	}
	lastId, _ = res.LastInsertId()
	rowCnt, _ = res.RowsAffected()
	log.Printf("Inserted into log successfully")
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)
}
