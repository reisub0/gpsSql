// This is a basic listener/adapter that parses incoming CSV GPS Data in the specific protocol and then
// Passes it to the gpsInserter function which is in the gpsInserter package
package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	evio "github.com/tidwall/evio"
	gpsInserter "gitlab.com/reisub0/gpsSql/gpsInserter"
)

const (
	// The IP/Port where the Legacy GPS Data is coming to
	LISTENHOST = "0.0.0.0"
	LISTENPORT = "8000"

	// SQL CRED
	// It is in the format user:password@tcp(127.0.0.1:3306)/dbname
	SQLCRED = "root:asdv@tcp(127.0.0.1:3306)/kudankulam"
)

var (
	db *sql.DB
	e  error
)

func main() {
	// If we get a Ctrl-C exit cleanly, this is to handle the SIGINT
	signalHandler()
	log.Printf("Runtime GoMAXPROCS = %d", runtime.GOMAXPROCS(0))

	log.Printf("Attempting to connect to Database: %s", SQLCRED)
	db, e = sql.Open("mysql", SQLCRED)
	// Check if db is connected
	e = db.Ping()
	if e != nil {
		log.Fatal(e)
	}

	var events evio.Events

	// Perform this action when the listen server starts
	events.Serving = func(srvin evio.Server) (_ evio.Action) {
		log.Printf("server started on port %s", LISTENPORT)
		return
	}

	// Perform this action whenever a new connection is received
	events.Opened = func(id int, info evio.Info) (_ []byte, _ evio.Options, _ evio.Action) {
		log.Printf("Connection %d launched %s -> %s", id, info.RemoteAddr, info.LocalAddr)
		return
	}

	// Perform this action whenever new data is received
	events.Data = dataHandler

	// Perform this action whenever a connection closes
	events.Closed = func(id int, _ error) (_ evio.Action) {
		log.Printf("Connection %d closed", id)
		return
	}

	// Start the server on LISTENHOST:LISTENPORT
	if err := evio.Serve(events, fmt.Sprintf("tcp://%s:%s", LISTENHOST, LISTENPORT)); err != nil {
		log.Panic(err.Error())
	}
}

// dataHandler is the function called asynchronously upon a new Data connection from a client
func dataHandler(id int, in []byte) (out []byte, action evio.Action) {

	// Assuming only messages terminated by newlines are valid
	message := strings.TrimRight(string(in), "\n")

	// Log the message for debugging
	log.Printf("Received message of length %d: %s", len(message), message)

	// Hand off the message pointer to gpsInserter to insert into SQL Database
	// This is the main function that does most of the work
	gpsInserter.Insert(&message, db)

	// We are done, we can return to let the garbage collector handle this stuff
	return
}

// Handle SIGINT by sending disconnect request to the MQTT Gateway Broker
func signalHandler() {
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, os.Interrupt)
	go func() {
		for sig := range sigchan {
			log.Printf("Server closing due to Signal: %s", sig)
			db.Close()
			os.Exit(0)
		}
	}()
}
